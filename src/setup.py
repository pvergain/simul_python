#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from __future__ import absolute_import, division, print_function

# Always prefer setuptools over distutils
from setuptools import setup

import os
import re

# Always prefer setuptools over distutils
from setuptools import setup

base_dir = os.path.dirname(__file__)

about = {}
with open(os.path.join(base_dir, "serial_id3" , "__about__.py")) as f:
    exec(f.read(), about)

with open(os.path.join(base_dir, "serial_id3", "README.rst")) as f:
    long_description = f.read()

with open(os.path.join(base_dir, "serial_id3", "CHANGELOG.rst")) as f:
    # Remove :issue:`ddd` tags that breaks the description rendering
    changelog = re.sub(r':issue:`\d+`', '', f.read())
    long_description = "\n".join([long_description, changelog])


setup(
    name=about["__title__"],
    version=about["__version__"],

    description=about["__summary__"],
    long_description=long_description,
    license=about["__license__"],
    url=about["__uri__"],

    author=about["__author__"],
    author_email=about["__email__"],

    install_requires=['pyserial'],

    classifiers=[
        "Intended Audience :: Developers",

        "License :: OSI Approved :: Apache Software License",
        "License :: OSI Approved :: BSD License",

        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
    ],

    packages = [
        "serial_id3",
    ],    
)

