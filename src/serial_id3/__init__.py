#!/usr/bin/env python
# -*- coding: UTF-8 -*-


from .__about__ import *
from .serial_id3 import *

__all__ = (serial_id3.__all__ + __about__.__all__)
