

==================================
Fichier CHANGELOG.rst 
==================================

.. seealso::

   - http://keepachangelog.com/
   - https://github.com/olivierlacan/keep-a-changelog
   
.. contents::
   :depth: 3
      

Préambule
==========

Il est important d'écrire un fichier CHANGELOG.rst décrivant les changements
entre chaque version.
Pour chaque version, on indiquera la date au format ISO8601 (i.e: YYYY-MM-JJ)
ou (["NON LIVREE", "unreleased","???", "IN DEVELOPMENT"] pour une version en 
cours de développement).


What’s a change log ?
=======================

A change log is a file which contains a curated, chronologically ordered list 
of notable changes for each version of a project.


What makes a good change log ?
===============================

I’m glad you asked.

A good change log sticks to these principles:

- It’s made for humans, not machines, so legibility is crucial.
- Easy to link to any section (hence Markdown over plain text).
- One sub-section per version.
- List releases in reverse-chronological order (newest on top).
- Write all dates in YYYY-MM-DD format. (Example: 2012-06-02 for June 2nd, 2012.) 
  It’s international, sensible, and language-independent.
- Explicitly mention whether the project follows Semantic Versioning.
- Each version should:

    - List its release date in the above format.
    - Group changes to describe their impact on the project, as follows:
    
        - ['Added', 'New features'] for new features.
        - Changed for changes in existing functionality.
        - Deprecated for once-stable features removed in upcoming releases.
        - Removed for deprecated features removed in this release.
        - Bugfixes for any bug fixes.
        - Security to invite users to upgrade in case of vulnerabilities.
        - Protocol
        - Protocol breaking changes
        - Miscellaneous
        - Updates
        - Minor Improvements (Backwards compatible)
        - Breaking changes
        - Internal changes
        - Deprecations and Removals
        - Behavioural Changes
        - API changes
        - Documentation
        
            - Highlights
            - Improved
            - Minor
        - Test


Exemples de fichiers CHANGELOG
===============================

- https://github.com/mozilla-services/cliquet/blob/signals/CHANGELOG.rst
- http://www.fabfile.org/changelog.html
- https://kinto.readthedocs.org/en/latest/changelog.html
- http://docs.python-requests.org/en/latest/community/updates/#release-and-version-history
- https://twistedmatrix.com/Releases/pre/16.0.0pre1/NEWS.txt
- https://github.com/olivierlacan/keep-a-changelog/blob/master/CHANGELOG.md

