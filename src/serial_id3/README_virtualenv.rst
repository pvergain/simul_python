
============
README.rst
===========

.. seealso::

   - http://docs.python-guide.org/en/latest/dev/virtualenvs/
   - https://virtualenv.pypa.io/en/latest/
   - https://github.com/pypa/virtualenv
   


Why 
====

A Virtual Environment is a tool to keep the dependencies required by different 
projects in separate places, by creating virtual Python environments for them. 

It solves the “Project X depends on version 1.x but, Project Y needs 4.x” 
dilemma, and keeps your global site-packages directory clean and manageable.


Glossaire Python
=================

.. seealso::

   - https://docs.python.org/3.6/glossary.html
   -


module
-------

.. seealso::

   - https://docs.python.org/3.6/glossary.html#term-module
   - http://www.dabeaz.com/modulepackage/ModulePackage.pdf

An object that serves as an organizational unit of Python code. 
Modules have a namespace containing arbitrary Python objects. 
Modules are loaded into Python by the process of importing.

See also package.


package
--------

.. seealso::

   - https://docs.python.org/3.6/glossary.html#term-package

A Python module which can contain submodules or recursively, subpackages. 
Technically, a package is a Python module with an __path__ attribute.

See also regular package and namespace package.

regular package
----------------

.. seealso:: https://docs.python.org/3.6/glossary.html#term-regular-package

A traditional package, such as a directory containing an __init__.py file.
See also namespace package.

namespace package
------------------

.. seealso:: https://docs.python.org/3.6/glossary.html#term-namespace-package

A PEP 420 package which serves only as a container for subpackages. 
Namespace packages may have no physical representation, and specifically are 
not like a regular package because they have no __init__.py file.

See also module.


Création dun environnement virtuel env_serial_35_64
====================================================

Création d'un environnement virtuel avec Python35 64 bits.

::

    mkvirtualenv env_serial_35_64 


Installation du module pyserial
=================================

.. seealso::

   - https://github.com/pyserial/pyserial
   - https://pyserial.readthedocs.org/en/latest/

Installation du module 'pyserial' dans l'environnement 'env_serial_35_64'.

::

    (env_serial_35_64) P5E627\XLOG9X324_python_Simul>pip install pyserial


::

    Collecting pyserial
    Using cached pyserial-3.0.1.tar.gz
    Building wheels for collected packages: pyserial
    Running setup.py bdist_wheel for pyserial ... done
    Stored in directory: C:\Users\pvergain\AppData\Local\pip\Cache\wheels\06\71\f1\d8c81329abcaf4c623daff8af76a7dbf00f055cb8205cec0ef
    Successfully built pyserial
    Installing collected packages: pyserial
    Successfully installed pyserial-3.0.1
    
   

Liste des modules installés
============================    

::

    pip list 
    
::

    pip (8.1.1)
    pyserial (3.0.1)
    setuptools (20.7.0)
    wheel (0.29.0)    


Sauvegarde des modules installés dans un fichier 'requirements.txt'
====================================================================


::

    pip freeze > requirements.txt
  
  
::

    (env_serial_35_64) C:\projects_id3\P5E627\XLOG9X324_python_Simul>type requirements.txt
    
::
    
    pyserial==3.0.1    
  
  
Pour activer un environnment virtuel Python
============================================

Taper::

    workon env_serial_35_64


ou (fortement recommandé) créer un fichier .bat et exécuter ce fichier::

    1_activate_env_serial_35_64.bat
        
    
    
Installation de modules complémentaires
=======================================

.. seealso::

   - https://ipython.org/index.html
   

Il est intéressant d'installer des modules complémentaires tels que 'ipython'
qui facilite la découverte de modules Python en ligne de commande.


::

    pip install ipython
    
::

    (env_serial_35_64) \P5E627\XLOG9X324_python_Simul>pip install ipython

::

    Collecting ipython
      Using cached ipython-4.1.2-py3-none-any.whl
    Collecting simplegeneric>0.8 (from ipython)
    Collecting pickleshare (from ipython)
      Downloading pickleshare-0.7.2-py2.py3-none-any.whl
    Collecting decorator (from ipython)
      Using cached decorator-4.0.9-py2.py3-none-any.whl
    Collecting traitlets (from ipython)
      Using cached traitlets-4.2.1-py2.py3-none-any.whl
    Requirement already satisfied (use --upgrade to upgrade): setuptools>=18.5 in c:\project\python_envs\env_serial_35_64\lib\site-packages (from ipython)
    Collecting ipython-genutils (from traitlets->ipython)
      Using cached ipython_genutils-0.1.0-py2.py3-none-any.whl
    Installing collected packages: simplegeneric, pickleshare, decorator, ipython-genutils, traitlets, ipython
    Successfully installed decorator-4.0.9 ipython-4.1.2 ipython-genutils-0.1.0 pickleshare-0.7.2 simplegeneric-0.8.1 traitlets-4.2.1
        
   
   
Connaitre la liste des packages à mettre à jour
================================================

::

    pip list -o
    

ou::

    pip list --outdated
    
            
Si un module est à mettre à jour, taper la commande::

    pip install -U <module_a_mettre_a_jour>            
            

Pour réinstaller un ensemble de modules
========================================

::

    pip install -r requirements.txt
    
    
                
    
    
    
