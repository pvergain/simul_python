

==================================
Documentation des projets Python   
==================================

.. seealso::

   - http://www.sphinx-doc.org/en/stable/
   - https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html
   - https://pvbookmarks.readthedocs.org/en/master/devel/documentation/doc_generators/sphinx/rest_sphinx/rest_sphinx.html
   

Introduction
============

Pour produire la documentation d'un projet Python, il faut installer le
package 'sphinx'::

    (env_serial_35_64) C:\projects_id3\P5E627\XLOG9X324_python_Simul>pip install sphinx


Les pages sont écrites au format reStructuredText (reST). 
Voir https://pvbookmarks.readthedocs.org/en/master/devel/documentation/doc_generators/sphinx/rest_sphinx/rest_sphinx.html    


En production, on pourra omettre l'installation de ce package.

    
Exemple de documentation sphinx à id3
=======================================

- file:///E:/users/pvergain/docs/PDEV5D092_doc_certis2_linux/html/index.html   
- file:///E:/users/pvergain/docs/id3_intranet/html/index.html

Pour un exemple de syntaxe reST, cliquer sur le bouton 'Show source'
de chaque page HTML.

ex: https://pvbookmarks.readthedocs.org/en/master/_sources/devel/documentation/doc_generators/sphinx/rest_sphinx/rest_sphinx.txt


Documentation d'une fonction avec ses paramètres
==================================================

Pour la documentation, utiliser la méthode recommandée par Google.
https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html

Exemple::

def beagle_records(filename):
    """Définition d'un générateur renvoyant un enregistrement 'beagle'.
        
    Note::
        En employant un générateur, on économise beaucoup de mémoire.

    Args:
        filename (str) : le nom complet du fichier.
        
    Yields:
        BeagleRecord: a beagle record.
        
    Raises:
        Exception: the filename does not exist.
            
    """
    print('opening the ',  filename , 'beagle file')
    try:
        for beagle_record in map(BeagleRecord._make, csv.reader(open(filename, "rb"))):
            yield beagle_record
    except Exception as e:
       logger.error('can t open file', filename)
        
 

Autres exemples de documentation sphinx 
=======================================

- https://www.python.org/ : la documentation de Python bien sûr
- http://sphinx-doc.org
  (source: https://github.com/sphinx-doc/sphinx)
- http://pytest.org/latest/
  (source : https://github.com/pytest-dev/pytest/tree/master/doc/en)
- https://pythonhosted.org/pyserial/
  (source: https://github.com/pyserial/pyserial/tree/master/documentation)  
- des milliers de projets sur https://readthedocs.org/

