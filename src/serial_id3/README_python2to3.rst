
=======================
README_python2to3.txt
=======================

.. seealso::

   - http://python-future.org/index.html
   - http://python-future.org/overview.html#futurize-2-to-both


Description
============

Pour avoir un code Python compatible Python2 et Python3, il faut utiliser
le module 'future' qui fournit l'utilitaire 'futurize'.


Pour l'installation
--------------------

::

    pip install future
    

Pour transformer un fichier 'Python2' en 'Python 2 et 3' compatible::


    futurize -w test_cl1356_aplus.py

::

    cd CL1356APlus
    
    futurize -w cl1356_aplus.py
    futurize -w mifare_access_conditions.py


    etc...
