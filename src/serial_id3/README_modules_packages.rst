

===========================================
Nommage des modules et packages en Python 
===========================================

.. seealso::

   - https://docs.python.org/3.6/glossary.html#term-package
   - https://docs.python.org/3.6/glossary.html#term-module
   


Introduction
============

Par convention, en Python le nom des modules et packages doit être en
minuscules.





Tutoriel de David Beazley
==========================

.. seealso::

   - https://github.com/dabeaz/modulepackage
   - http://www.dabeaz.com/modulepackage/ModulePackage.pdf


Le tutoriel de David Beazley est à lire !

Modules and Packages : Live and Let Die

Tutorial Presentation at PyCon'2015. April 9, 2015. Montreal.

David Beazley (@dabeaz), http://www.dabeaz.com

This tutorial assumes the use of Python 3.4 or newer. Certain examples in 
sections 8 and 9 require the use of Python 3.5.

The official website for this tutorial is http://www.dabeaz.com/modulepackage/index.html


Naming Conventions
-------------------

.. seealso::    

   - http://www.dabeaz.com/modulepackage/ModulePackage.pdf

- It is standard practice for package and module names to be concise and 
  lowercase
  
  Ex: foo.py not MyFooModule.py

- Use a leading underscore for modules that are meant to be private or 
  internal

- Don't use names that match common standard library modules (confusing)



Utilisation de __all__
------------------------

Exemple::

    from .__about__ import *

    from .bits_fields import *
    from .convertion import *
    from .id3_enum import *
    from .simple_tlv import *
    from .convertion_container import *

    from .exception import *
    from .reader import *
    from .pcsc_enums import *
    from .reader_state import *
    from .card_states import *
    from .iso_15693 import *
    from .mifare import *
    from .mifare_access_conditions import *
    from .apdu_commands import *
    from .parse_atr import *
    from .sanitize import *
    from .os_util import *


    # see http://www.dabeaz.com/modulepackage/ModulePackage.pdf
    __all__ = (__about__.__all__ + bits_fields.__all__ + convertion.__all__ +
               id3_enum.__all__ + simple_tlv.__all__ + convertion_container.__all__
               + exception.__all__ + reader.__all__ + pcsc_enums.__all__ +
               reader_state.__all__ + card_states.__all__ + iso_15693.__all__ +
               mifare.__all__ + mifare_access_conditions.__all__ +
               apdu_commands.__all__ + parse_atr.__all__ +
               sanitize.__all__ + os_util.__all__)


Part 1 - Basic Knowledge
-------------------------

basic_package/ : A very simple package consisting of multiple files.


- https://github.com/dabeaz/modulepackage/tree/master/basic_package/spam


Part 2 - Packages
------------------

- https://github.com/dabeaz/modulepackage/tree/master/package_assembly/spam 
  An example of assembling a package from submodules by exporting symbols 
  in __init__.py files.

- https://github.com/dabeaz/modulepackage/tree/master/decorator_assembly/spam 
  decorator_assembly/: Assemble a package from submodules using a 
  special @export decortor.


Part 3 - main
---------------

- https://github.com/dabeaz/modulepackage/tree/master/main_wrapper 
  main_wrapper : An example of writing a module that wraps around a script 
  using the -m option.


Part 4 - sys.path
-------------------

No code samples for this part.


Part 5 - Namespace Packages
----------------------------

- https://github.com/dabeaz/modulepackage/tree/master/namespace_package
  namespace_package : A simple namespace package example.

- https://github.com/dabeaz/modulepackage/tree/master/telly
  telly : A package that uses namespace packages to allow for user-extensible 
  submodules.


Part 6 - The Module
---------------------

mini_import : A minimalistic implementation of the import statement.

subpackage_cycle : A package where submodules import each other in a cycle.

import_patch : An example of patching the builtin __import__() function.


Part 7 - The Module Reloaded
-----------------------------

reload_instances : A class that detects reloading and updates instances.

reload_func : A context manager that allows reloadable functions to be declared.


Part 8 - Import Hooks
-----------------------

trial_import : A module that tests for another module with import

check_import : A module that tests for another module using module specs.

lazy_import : A module that only executes when accessed for the first time.

watcher : A simple import hook.

autoinstall : An import hook that automatically installs missing modules.

redisimport : An import hook that loads modules from Redis.


Part 9 - Path Hooks
--------------------

webhook : An import hook that allows URLs to be placed on sys.path.


