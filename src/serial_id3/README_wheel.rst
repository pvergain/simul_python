

====================
Python wheel
====================

.. seealso::

   - http://pythonwheels.com/
   - https://www.python.org/dev/peps/pep-0427/
   - https://packaging.python.org/en/latest/glossary/#term-wheel
   - https://packaging.python.org/en/latest/distributing/#wheels
   

What are wheels ?
===================

Wheels are the new standard of python distribution and are intended to replace 
eggs. Support is offered in pip >= 1.4 and setuptools >= 0.8.

Advantages of wheels

- Faster installation for pure python and native C extension packages.
- Avoids arbitrary code execution for installation. (Avoids setup.py)
- Installation of a C extension does not require a compiler on Windows or OS X.
- Allows better caching for testing and continuous integration.
- Creates .pyc files as part of installation to ensure they match the python 
  interpreter used.
- More consistent installs across platforms and machines.



Les wheels pour windows
========================

.. seealso::

   - www.lfd.uci.edu/~gohlke/pythonlibs/

Grâce Christoph Gohlke à on peut trouver un grand nombre de wheels pour windows 
ici: www.lfd.uci.edu/~gohlke/pythonlibs/


This page provides 32- and 64-bit Windows binaries of many scientific 
open-source extension packages for the official CPython distribution of 
the Python programming language.

The files are unofficial (meaning: informal, unrecognized, personal, 
unsupported, no warranty, no liability, provided "as is") and made available 
for testing and evaluation purposes.

Exemples d'installation de wheels
==================================

pywin32
--------

Pour installer le package pywin32, il suffit de taper la commande::

    pip install -U pywin32-220-cp35-none-win_amd64.whl
   
   
pyscard
-------

- https://ci.appveyor.com/project/LudovicRousseau/pyscard

Architecture 64 bits, python 3.4:

- voir https://ci.appveyor.com/project/LudovicRousseau/pyscard/build/job/5ak0bf4gydihc0r5/artifacts    
 
 
 
wheels à id3
-------------

Voir:

- /export/disk0/svn/P7Q021/Concept/Soft/PC/XLOGAA334_wheel_python_packages
- /export/disk0/svn/P7Q021/Concept/Soft/PC/XLOGAA307_pycl1356_python_package/trunk
- /export/disk0/svn/P7Q021/Concept/Soft/PC/XLOG8V053_pycl1356a_python_package/trunk
- /export/disk0/svn/P7Q021/Concept/Soft/PC/XLOG1X318_pycl1356_aplus_python_package/trunk
- /export/disk0/svn/P1V160/Concept/Soft/PC/Python/trunk/XLOG1Y043_Easytest2PythonPackage

Distribution de ses programmes Python 
======================================

.. seealso::

   - https://packaging.python.org/en/latest/distributing/#wheels

Il est possible de distribuer ces programmes sous forme de wheel.

You should also create a wheel for your project. 

A wheel is a built package that can be installed without needing to go through 
the “build” process. Installing wheels is substantially faster for the end user 
than installing from a source distribution.

If your project is pure python (i.e. contains no compiled extensions) and 
natively supports both Python 2 and 3, then you’ll be creating what’s called 
a “Universal Wheel” (see section below).

If your project is pure python but does not natively support both 
Python 2 and 3, then you’ll be creating a “Pure Python Wheel” (see section below).

If you project contains compiled extensions, then you’ll be creating what’s 
called a “Platform Wheel” (see section below).




 
   
   
