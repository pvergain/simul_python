

==================================
Test des projets Python   
==================================

.. seealso::

   - http://pytest.org/latest/
   

Introduction
============

Pour tester un projet Python, il faut installer le package 'pytest' qui est
le plus pythonique des modules de tests Python.


Description
===========


pytest: helps you write better programs

a mature full-featured Python testing tool

- runs on Posix/Windows, Python 2.6-3.5, PyPy and (possibly still) Jython-2.5.1
- free and open source software, distributed under the terms of the MIT license
- well tested with more than a thousand tests against itself
- strict backward compatibility policy for safe pytest upgrades
- comprehensive online and PDF documentation
- many third party plugins and builtin helpers,
- used in many small and large projects and organisations
- comes with many tested examples


pytest has support for running Python unittest.py style tests. 
It’s meant for leveraging existing unittest-style projects to use pytest 
features. 

Concretely, pytest will automatically collect unittest.TestCase subclasses and 
their test methods in test files. It will invoke typical setup/teardown methods 
and generally try to make test suites written to run on unittest, to also run 
using pytest. 

We assume here that you are familiar with writing unittest.TestCase style 
tests and rather focus on integration aspects.




Installation 
============


::

    (env_serial_35_64) C:\projects_id3\P5E627\XLOG9X324_python_Simul>pip install pytest
    
::
    
    Collecting pytest
      Using cached pytest-2.9.1-py2.py3-none-any.whl
    Collecting py>=1.4.29 (from pytest)
      Using cached py-1.4.31-py2.py3-none-any.whl
    Requirement already satisfied (use --upgrade to upgrade): colorama in c:\project\python_envs\env_serial_35_64\lib\site-packages (from pytest)
    Installing collected packages: py, pytest
    Successfully installed py-1.4.31 pytest-2.9.1      
        


Pour lancer les tests
======================

::

    py.test test\test_port5.py
    

::


    test\test_port5.py .F

    ================================== FAILURES ===================================
    _______________________________ test_open_port5 _______________________________

        def test_open_port5():
            ser = None
            try:
                print("Opening serial port {}...".format(serial_port))
                port_com = 'COM'+ str(serial_port-1)
                ser = serial.Serial(port_com, 115200)
                print(ser.name)
                print("...was sucessfull")
                assert ser is not None
            except serial.SerialException as e :
                print("Unable to open COM port {}, please check port number".format(port_com))
    >           assert ser is not None
    E           assert None is not None

    test\test_port5.py:43: AssertionError
    ---------------------------- Captured stdout call -----------------------------
    Opening serial port 5...
    Unable to open COM port COM4, please check port number
    ===================== 1 failed, 1 passed in 0.05 seconds ======================    
        
    
    
