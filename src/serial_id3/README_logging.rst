

====================
Python logging
====================

.. seealso::

   - http://pythonwheels.com/
   - https://www.python.org/dev/peps/pep-0427/
   - https://packaging.python.org/en/latest/glossary/#term-wheel
   - https://packaging.python.org/en/latest/distributing/#wheels
   

Exemple de code Python
=======================

Initialisation du code de logging.

::

    # ===============================================
    # CRITICAL  50  Le programme complet agonise.
    # ERROR     40  Une opération a foirée.
    # WARNING   30  Pour avertir que quelque chose mérite l’attention
    #               enclenchement d’un mode particulier, detection d’une situation
    #               rare, un lib optionelle peut être installée.
    # INFO      20  Pour informer de la marche du programme.
    #               Par exemple : “Starting CSV parsing”
    # DEBUG     10  Pour dumper des information quand vous débuggez.
    import logging
    from logging.handlers import RotatingFileHandler
    
    # création de l'objet logger qui va nous servir à écrire dans les logs    
    logger = logging.getLogger()
    
    # on met le niveau du logger à DEBUG, comme ça il écrit tout
    logger.setLevel(logging.DEBUG)
    # création d'un formateur qui va ajouter le temps, le niveau
    # de chaque message quand on écrira un message dans le log
    formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
    # création d'un handler qui va rediriger une écriture du log vers
    # un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
    file_handler = RotatingFileHandler('test_pyscard.log', 'a', 1000000, 1)
    # on lui met le niveau sur DEBUG donc tous les messages
    # de log vont se retrouver dans le fichier de logs
    file_handler.setLevel(logging.DEBUG)
    # si on met "INFO", les messages de DEBUG ne seront pas dans le fichier de log
    # file_handler.setLevel(logging.INFO)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    # création d'un second handler qui va rediriger chaque écriture de log
    # sur la console
    stream_handler = logging.StreamHandler()
    stream_handler.setLevel(logging.INFO)
    # stream_handler.setFormatter(formatter)
    logger.addHandler(stream_handler)
    
    
Exemples d'appel
=================

::

    def test_scard_error_message():
        """Return the error string corresponding to the num error

        """
        try:
            for i, error_code in enumerate(RETURN_CODES):
                error_message = get_scard_error_message(error_code.Value)
                ascii_message = asciize(error_message)
                logger.info("{} {} ==>  {} {}".format(i+1, 
                                                   error_code.Value, 
                                                   error_message,
                                                   ascii_message))
            logger.info("\n\n")                                               
        except Exception as error:
            logger.error(error)
        finally:
            pass    
        
  
  
::

    def test_toggle_led():
        """Test toggle the LED.

        """
        logger.info("Begin test_toggle_led()")
        try:
            Reader = CL1356Reader()
            Reader.establish_context()
            reader_names_list = CL1356Reader.list_readers()
            if len(reader_names_list) < 1:
                raise IOError("No smart card readers")
            else:
                for reader_name in reader_names_list:
                    Reader.connect(reader_name)
                    _, State, ATR = Reader.get_card_status()
                    print("ReaderName:{}".format(reader_name))
                    if ("CL1356A+" in reader_name):
                        Reader.transmit(APDU_COMMANDS_CL1356A_PLUS['TOGGLE_LED'])
                    else:
                        Reader.transmit(APDU_COMMANDS_CL1356['TOGGLE_LED'])

                    Reader.disconnect()

        except Exception as error_exception:
            logger.error(error_exception)
        finally:
            Reader.release_context()
            logger.info("End test_toggle_led()")  
      
      
      
Les logs dans les fichiers
===========================

::

    2016-02-08 11:26:59,880 :: INFO :: Begin test_toggle_led()
    2016-02-08 11:27:00,085 :: INFO :: End test_toggle_led()
    2016-02-08 11:27:00,092 :: INFO :: CL1356Exception: connect returns 0x00000001(ERROR_INVALID_FUNCTION) / Missing context handle. EstablishedContext must be called first : Missing context handle. EstablishedContext must be called first
    2016-02-08 11:27:00,096 :: INFO :: error_exception.error_code:('SCARD_E_UNKNOWN_READER', -2146435063L)
    2016-02-08 11:32:12,983 :: INFO :: Begin test_toggle_led()
    2016-02-08 11:32:13,184 :: INFO :: End test_toggle_led()
    2016-02-08 11:32:13,206 :: INFO :: Begin test_connect_methods()
    2016-02-08 11:32:13,206 :: INFO :: CL1356Exception: connect returns 0x00000001(ERROR_INVALID_FUNCTION) / Missing context handle. EstablishedContext must be called first : Missing context handle. EstablishedContext must be called first
    2016-02-08 11:32:13,210 :: INFO :: error_exception.error_code:('SCARD_E_UNKNOWN_READER', -2146435063L)
    2016-02-08 11:32:13,210 :: INFO :: test_connect_methods() successful
    2016-02-08 11:32:13,217 :: ERROR :: CL1356Exception: SCardControl returns 0x00000001(ERROR_INVALID_FUNCTION)
    2016-02-08 11:32:28,021 :: INFO :: Begin test_toggle_led()
            
    
    
