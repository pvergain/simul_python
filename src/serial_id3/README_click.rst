

====================================================================
Appel de programme Python en ligne de commande avec des paramètres
====================================================================

.. seealso::

   - http://click.pocoo.org
   - http://click.pocoo.org/6/changelog/


Description
============

Click is a Python package for creating beautiful command line interfaces in a 
composable way with as little code as necessary. 

It’s the “Command Line Interface Creation Kit”. 
It’s highly configurable but comes with sensible defaults out of the box.

It aims to make the process of writing command line tools quick and fun while 
also preventing any frustration caused by the inability to implement an 
intended CLI API.


Introduction
============

Le module python 'click' est le module le plus facile à utiliser pour passer
des arguments à un programme Python.

Exemple::

    import click  # http://click.pocoo.org (pip install click)


    @click.command()
    # CLI Command Line Interface (see http://click.pocoo.org)
    @click.option('--database_source_type', type=click.Choice(['production', 'stage']), default='stage' , help='The database source type')
    @click.option('--language', default='en' , help='The language used for the mail')
    # The 3 boolean flags (see http://click.pocoo.org/3/options/#boolean-flags)
    @click.option('--send_mail/--no_send_mail', is_flag=True, default=False, help='Send email(s) to the list of recipients')
    @click.option('--add_client_mail/--no_add_client_mail', is_flag=True, default=False , help='Add the client email to the list of recipients')
    @click.option('--stdout/--no_stdout', is_flag=True, default=True , help='Print information on the standard output')
    def main(database_source_type, language, send_mail, add_client_mail, stdout): 
        '''
            
        '''
        try:        
            parameters = init_parameters()
            log_handler = get_log_handler(parameters['logs']['directory'])
        except Exception as e:
            print('pb with the log system {}'.format(e))
            return 
                        
        with log_handler.applicationbound():
            try:
                logger.info('Starting log processing...')             
                message = "\n\nBegin {}\n\tdatabase_source_type:{}\n\tlanguage:{}\n\tsend_mail:{}\n\tadd_client_mail:{}\n\tstdout:{} "\
                    .format(__file__, 
                    database_source_type ,
                    language,
                    send_mail, add_client_mail,
                    stdout)
                if stdout:
                    print(message)

                                
                logger.info(message)
                test_exist_db_env_variables()              
                
                # Get the jinja2 templates mail for the choosen language             
                subject_mail_template , body_mail_template = get_mail_templates(language)
                        
                # Add the 7 parameters:
                parameters['database_source_type'] = database_source_type
                parameters['language'] = language            
                parameters['subject_mail_template'] = subject_mail_template            
                parameters['body_mail_template'] = body_mail_template            

                parameters['send_mail'] = send_mail        
                parameters['add_client_mail'] = add_client_mail
                parameters['stdout'] = stdout
                    
                database = init_database(parameters)
                for sql_view in parameters['sql']['list_sql_views']:
                    report_licenses_expired_soon(parameters, database, sql_view)
                    
                    
            except Exception as e:
                logger.error('{}'.format(e)) 
            finally:
                logger.info("End main()")              
     
     
    if __name__ == '__main__':
        main()

