

====================
Python Code quality
====================

.. seealso::

   - http://docs.python-guide.org/en/latest/writing/style/
   

Introduction
============

Pour améliorer la qualité du code Python, il est conseillé d'avoir un 
environnement virtuel 'python_quality' contenant les packages suivants:

- autopep8
- pydocstyle
- pylint
- isort


autopep8
=========

.. seealso:: https://github.com/hhatto/autopep8


::

    autopep8 <module.py>
   
   
pydocstyle
===========

.. seealso:: https://github.com/PyCQA/pydocstyle

    
::

    pydocstyle <module.py>

.. note:: pour commenter les arguments des fonctions Python, employer
   le style recommandé par Google.
   
   Voir: https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html
   
  

pylint
=======

.. seealso:: https://github.com/PyCQA/pylint
    
::

    pylint <module.py>
            

isort
======

.. seealso:: https://github.com/timothycrosley/isort


::

    isort <module.py>





