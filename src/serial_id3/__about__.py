#!/usr/bin/env python
# -*- coding: UTF-8 -*-


# see https://github.com/pypa/packaging/blob/master/packaging/__about__.py
from __future__ import absolute_import, division, print_function

__all__ = (
    "__title__", "__summary__", "__uri__", "__version__", "__author__",
    "__email__", "__license__", "__copyright__",
)

__title__ = "serial id3"
__summary__ = "Exemple d'utilisation du module serial"

__version__ = "0.2.0"

__uri__ = "https://www.id3.eu"

__author__ = "id3 Technologiess"
__email__ = "hamzah.amara@id3.eu"

__license__ = "BSD or Apache License, Version 2.0"
__copyright__ = "Copyright 2008-2016 %s" % __author__
