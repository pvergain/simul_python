
============
Versions
============


0.2.0 (IN DEVELOPMENT)
=======================

Documentation
--------------

- utilisation de la méthode recommandée par Google pour commenter les
  paramètres de fonctions.
  https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html

- ajout du fichier CHANGELOG.rst
- ajout de fichiers README_{click,doc,logging,modules_packages,python2to3,
                           quality, test, virtualenv, wheel} 
  pour transmettres les récentes bonnes pratiques en matière de 
  développment Python.

Internal changes
-----------------

- utilisation du package logging
- utilisation du package click
- utilisation de __all__ dans le module __init__.py

Test
-----

- création d'un répertoire 'test'.
- utilisation du framework pytest


0.1.0 (2010-11-5)
====================

First release
