#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""This module simulates the input of case frames by reading a file of records.

captured by the `Beagle USB Protocol Analyzer <http://www.totalphase.com/support/product/beagle_ism/>`_

- the fake frames are sent to a COM5 port.
- the reader (not provided here) reads a COM1 port.

Note:
    Pour la documentation, utiliser la méthode recommandée par Google.
    https://sphinxcontrib-napoleon.readthedocs.org/en/latest/example_google.html

    except serial.SerialException as e :
        print("Unable to open COM port %d, please check port number" % COMPortNumber)

"""

# Import addons modules
import serial
from serial.tools import list_ports

serial_port = 5
"""int: serial_port number

Spécifier le numéro de port serie en fonction de votre PC.
"""

def test_exist_port():
     list_ports

def test_open_port5():
    ser = None
    try:
        print("Opening serial port {}...".format(serial_port))
        port_com = 'COM'+ str(serial_port-1)
        ser = serial.Serial(port_com, 115200)
        print(ser.name) 
        print("...was sucessfull")
        assert ser is not None
    except serial.SerialException as e :
        print("Unable to open COM port {}, please check port number".format(port_com))
        assert ser is not None        
    finally:
        if ser is not None:
            ser.close()
